#!/bin/zsh
rm -rdf build_clang build_gcc build
cmake -S src -B build_clang -D CMAKE_C_COMPILER=clang
cmake -S src -B build_gcc -D CMAKE_C_COMPILER=gcc

cd build_clang
make
./memalloc

cd ..

cd build_gcc
make
./memalloc
rm -rdf build_clang build_gcc build
