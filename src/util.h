#ifndef _UTIL_H_
#define _UTIL_H_

#include <stddef.h>

inline size_t size_max(size_t const x, size_t const y) {
  return (x >= y) ? x : y;
}
inline size_t size_min(size_t const x, size_t const y) {
  return (x >= y) ? y : x;
}

_Noreturn void err(const char *msg, ...);

#endif
