#ifndef _MEM_INTERNALS_
#define _MEM_INTERNALS_

#include <inttypes.h>
#include <stdbool.h>
#include <stddef.h>

#define REGION_MIN_SIZE (2 * 4096)

struct region {
  void *addr;
  size_t size;
  bool extends;
};

static const struct region REGION_INVALID = {0};

inline bool region_is_invalid(struct region const *const r) {
  return r->addr == NULL;
}

typedef struct {
  size_t bytes;
} block_capacity;
typedef struct {
  size_t bytes;
} block_size;

struct block_header {
  struct block_header *next;
  block_capacity capacity;
  bool is_free;
  uint8_t contents[];
};

inline block_size size_from_capacity(block_capacity const capacity) {
  return (block_size){capacity.bytes + offsetof(struct block_header, contents)};
}
inline block_capacity capacity_from_size(block_size const size) {
  return (block_capacity){size.bytes - offsetof(struct block_header, contents)};
}

void *memcpy_sidestep_linter_bug(void *dest, void const *source, size_t size);
void *header_transfer(struct block_header const *from, struct block_header *to);
struct block_header header_extract(struct block_header const *from);

struct block_header *get_header_from_block(void *block);
const struct block_header *block_after(struct block_header const *block);

#endif
