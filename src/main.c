#include <assert.h>
#include <linux/mman.h>
#include <stdlib.h>
#include <string.h>

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

#define HEAP_SIZE (2 << 10)
#define BLOCK_SIZE (2 << 8)

static_assert(HEAP_SIZE > BLOCK_SIZE, "Tests assume that");
static_assert(BLOCK_SIZE > BLOCK_MIN_CAPACITY, "Tests assume that");

/* NOTE: check regex: `[^_]malloc` and `\sfree` */

int main(void) {
  void *heap = heap_init(HEAP_SIZE);

  if (heap) {
    void *block1 = _malloc(BLOCK_SIZE);
    if (block1) {
      puts("Successful block allocation");
      puts("Testing block characteristics");
      {
        struct block_header header = {0};
        struct block_header free_header = {0};
        memcpy_sidestep_linter_bug(&header, get_header_from_block(block1),
                                   sizeof(struct block_header));
        memcpy_sidestep_linter_bug(&free_header, header.next,
                                   sizeof(struct block_header));
        if (header.capacity.bytes != BLOCK_SIZE || header.is_free ||
            !free_header.is_free) {
          puts("Block unhealthy. Free status off.");
          return 1;
        }
      }
      puts("Block characteristics found healthy");
      puts("Test1 succesful");

      void *block2 = _malloc(BLOCK_SIZE);
      void *block3 = _malloc(BLOCK_SIZE);
      if (block2 && block3) {
        {
          puts("Freeing a block");
          _free(block2);

          {
            struct block_header header2 = {0};
            memcpy_sidestep_linter_bug(&header2, get_header_from_block(block2),
                                       sizeof(struct block_header));
            if (!header2.is_free) {
              puts("Block free failed");
              return 1;
            }
          }
          puts("One block of the three successfuly freed");
          puts("Test2 successful");

          _free(block1);
          {
            struct block_header header1 = {0};
            memcpy_sidestep_linter_bug(&header1, get_header_from_block(block1),
                                       sizeof(struct block_header));
            if (!header1.is_free ||
                header1.next != get_header_from_block(block3)) {
              puts("Block free failed");
              return 1;
            }
          }
          puts("Two blocks successfuly freed");
          puts("Test3 successful");
          _free(block3);

          block1 = _malloc(2 * HEAP_SIZE);

          if (block1) {
            struct block_header header = {0};
            memcpy_sidestep_linter_bug(&header, get_header_from_block(block1),
                                       sizeof(struct block_header));

            if (header.capacity.bytes != 2 * HEAP_SIZE) {
              puts("Failed heap growth for test4");
              return 1;
            }
            puts("Test4 successful");

            void *void_pointer = mmap(
                ((void*)block_after(header.next)), HEAP_SIZE, PROT_READ | PROT_WRITE,
                MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, -1, 0);
            if (!void_pointer) {
              puts("Failed mmap allocation for the fifth test");
              return 1;
            }
            block2 = _malloc(4 * HEAP_SIZE);
            if (!block2) {
              puts("Failed test5 allocation");
            }

            puts("Test5 successful");
            puts("All 5 tests passed seccussfuly");
            _free(block1);
            _free(block2);
            return 0;
          }
        }
      }
    }
    puts("Block allocation failure");
    return 1;
  }

  puts("Heap init allocation failure");
  return 1;
}
