#define _DEFAULT_SOURCE

/* Disclaimer: I don't think I possess the necessary
 * skills and and understanding of the processes involved
 * to achieve the desired brevity here. But I did my best
 * to make a malloc implementation here that will not
 * cause any unaligned accesses and the code was tested
 * with fsanitize=alignment.*/

/* TODO: replace all the in-code
 * header reading code with some function*/

#include "mem.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "mem_internals.h"
#include "util.h"

#define MEMORY_ZEROING_ENABLED

extern inline block_size size_from_capacity(block_capacity const capacity);
extern inline block_capacity capacity_from_size(block_size const size);

void *memcpy_sidestep_linter_bug(void *const dest, void const *const source,
                                 size_t const size) {
  unsigned char *const casted_dest = (unsigned char *)dest;
  unsigned char const *const casted_source = (unsigned char const *)source;
  for (size_t i = 0; i < size; i++) {
    casted_dest[i] = casted_source[i];
  }
  return dest;
}

void *header_transfer(struct block_header const *const from,
                      struct block_header *const to) {
  /* Annex K is dead*/
  return memcpy(to, from, sizeof(struct block_header)); // NOLINT
}

struct block_header header_extract(struct block_header const *const from) {
  struct block_header header = {0};
  header_transfer(from, &header);
  return header;
}

static bool block_is_big_enough(size_t const query,
                                struct block_header const *const block) {
  struct block_header header = header_extract(block);
  return header.capacity.bytes >= query;
}

struct block_header *get_header_from_block(void *const block) {
  return (struct block_header *)(((uint8_t *)block) -
                                 offsetof(struct block_header, contents));
}

/* NOTE to self
 * getpagesize() returns an int
 * however the man page does not indicate that it could
 * ever retrun a negative value
 * yet nonetheless further research into
 * such a possibility shall be done
 * and perhaps a precaution likes of
 *  max(0, (size_t)getpagesize())
 * could be employed*/
static inline size_t getpagesize_wrapper(void) { return (size_t)getpagesize(); }

static size_t pages_count(size_t const mem) {
  /* TODO: consider using shifts in here
   * although I suspect that the danger of portability
   * degradation would arise due to
   * a platform pagesize not being a power of two */
  return mem / getpagesize_wrapper() + ((mem % getpagesize_wrapper()) > 0);
}
static size_t round_pages(size_t const mem) {
  return getpagesize_wrapper() * pages_count(mem);
}

static struct block_header *
block_init(void *restrict const address, block_size const block_size,
           struct block_header *restrict const next) {
  /* NOTE to future self
   * there is no behavour that makes any sense
   * if we are asked to init a block
   * but are not given enough space to init a block
   * hence the if */
  if (address && block_size.bytes >= sizeof(struct block_header)) {
    struct block_header header = {0};
    header.next = next;
    header.capacity = capacity_from_size(block_size);
    header.is_free = true;
    return (struct block_header *)header_transfer(&header, address);
  }
  return NULL;
}

static size_t region_actual_size(size_t const query) {
  return size_max(round_pages(query), REGION_MIN_SIZE);
}

/* TODO: consider hugepages use
 * perhaps behind a branch qualifying
 * sufficiently large allocations*/
static void *map_pages(void *const addr, size_t const length,
                       int const additional_flags) {
  return mmap(addr, length, PROT_READ | PROT_WRITE,
              MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(void *const address,
                                  size_t const original_query) {
  size_t const query = region_actual_size(
      size_from_capacity((block_capacity){.bytes = original_query}).bytes);

  void *region_pointer = map_pages(address, query, MAP_FIXED_NOREPLACE);

  if (region_pointer == MAP_FAILED || !region_pointer) {
    region_pointer = map_pages(address, query, 0);
    if (region_pointer == MAP_FAILED || !region_pointer) {
      return (struct region){0};
    }
  }
  if (region_pointer) {
    /* if is redundent,
     * yet lack of it spikes my paranoia
     * TODO: try to rearrange the ifs in here to make
     * the code assuring enough not to spike any paranoia */
    block_init(region_pointer, (block_size){.bytes = query}, NULL);
  }

  /* TODO: can I use MAX_FIXED_NOREPLACE to make
   * a better check for the .extends value?
   * perhaps by checking that the previous byte is taken
   * by some other allocation? */
  return (struct region){.addr = region_pointer,
                         .size = query,
                         .extends = (region_pointer == address)};
}

/* TODO: what is this?
static void *header(struct block_header const *block);*/

void *heap_init(size_t const initial) {
  return alloc_region(HEAP_START, initial).addr;
}

static bool block_splittable(struct block_header *restrict const block,
                             size_t const query) {
  struct block_header header = header_extract(block);
  return header.is_free &&
         query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <=
             header.capacity.bytes;
}

static bool split_if_too_big(struct block_header *const block,
                             size_t const query) {
  if (block_splittable(block, query)) {
    struct block_header header = header_extract(block);

    header.next =
        block_init((void *)((uint8_t *)block +
                            offsetof(struct block_header, contents) + query),
                   (block_size){header.capacity.bytes - query}, header.next);

    header.capacity.bytes = query;
    header_transfer(&header, block);
    return true;
  }
  return false;
}

/*  --- Слияние соседних свободных блоков --- */
/* TODO: casts here drop const qualifiers
 * could do with looking into those warnings */
struct block_header const *
block_after(struct block_header const *const block) {
  struct block_header header = header_extract(block);
  return (void const *)((uint8_t const *)block +
                        offsetof(struct block_header, contents) +
                        header.capacity.bytes);
}
static bool blocks_continuous(struct block_header const *const first,
                              struct block_header const *const second) {
  if (first && second) {
    return (void const *)second == block_after(first);
  }
  return false;
}

static bool mergeable(struct block_header *restrict const first,
                      struct block_header const *restrict const second) {
  if (first && second) {
    struct block_header header1 = header_extract(first);
    struct block_header header2 = header_extract(second);
    return header1.is_free && header2.is_free &&
           blocks_continuous(first, second);
  }
  return false;
}

static bool try_merge_with_next(struct block_header *const block) {
  if (block) {
    struct block_header header = header_extract(block);

    if (header.next) {
      if (mergeable(block, header.next)) {
        struct block_header nextHeader = header_extract(header.next);

        header.capacity.bytes += size_from_capacity(nextHeader.capacity).bytes;
        header.next = nextHeader.next;

        header_transfer(&header, block);
        return true;
      }
    }
  }
  return false;
}

/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum { BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED } type;
  struct block_header *block;
};

static struct block_search_result
find_good_or_last(struct block_header *restrict const start,
                  size_t const size) {
  if (start) {
    struct block_header *current = start;
    while (current) {
      while (try_merge_with_next(current)) {
      }
      struct block_header header = header_extract(current);
      if (header.is_free && block_is_big_enough(size, current)) {
        return (struct block_search_result){.type = BSR_FOUND_GOOD_BLOCK,
                                            .block = current};
      }
      if (header.next)
        current = header.next;
      else
        break;
    }
    return (struct block_search_result){.type = BSR_REACHED_END_NOT_FOUND,
                                        .block = current};
  }
  return (struct block_search_result){.type = BSR_CORRUPTED, .block = NULL};
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь
 расширить кучу Можно переиспользовать как только кучу расширили. */
static struct block_search_result
try_memalloc_existing(size_t const original_query,
                      struct block_header *const block) {
  size_t const query = size_max(original_query, BLOCK_MIN_CAPACITY);
  struct block_search_result result = find_good_or_last(block, query);

  if (result.type != BSR_FOUND_GOOD_BLOCK) {
    return result;
  }
  split_if_too_big(result.block, query);

  struct block_header header = header_extract(result.block);
  header.is_free = false;
  header_transfer(&header, result.block);
  return result;
}

static struct block_header *grow_heap(struct block_header *restrict const last,
                                      size_t const query) {
  if (last) {
    struct region region = alloc_region(((void*)block_after(last)), query);
    if (region.addr) {
      struct block_header header = header_extract(last);
      header.next = region.addr;
      header_transfer(&header, last);

      if (header.is_free) {
        if (region.extends) {
          while (try_merge_with_next(last)) {
          }
          return last;
        }
      }
      return region.addr;
    }
  }
  return NULL;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header *memalloc(size_t const original_query,
                                     struct block_header *const heap_start) {
  if (heap_start) {
    size_t const query = size_max(original_query, BLOCK_MIN_CAPACITY);
    struct block_search_result result =
        try_memalloc_existing(query, heap_start);
    if (result.type == BSR_REACHED_END_NOT_FOUND) {
      struct block_header *new_block = grow_heap(result.block, query);
      if (!new_block)
        return NULL;
      result = try_memalloc_existing(query, new_block);
    }
    if (result.type == BSR_FOUND_GOOD_BLOCK) {
      return result.block;
    }
  }
  return NULL;
}

void *_malloc(size_t const query) {
  struct block_header *const addr =
      memalloc(query, (struct block_header *)HEAP_START);
  if (addr) {
#ifdef MEMORY_ZEROING_ENABLED
    struct block_header header = header_extract(addr);
    explicit_bzero((uint8_t *)addr + offsetof(struct block_header, contents),
                   header.capacity.bytes);
#endif
    return (void *)((uint8_t *)addr + offsetof(struct block_header, contents));
  } else
    return NULL;
}

void _free(void *const block) {
  if (block) {
    struct block_header *header = get_header_from_block(block);

    struct block_header header_copy = header_extract(header);
    header_copy.is_free = true;
    header_transfer(&header_copy, header);

    while (try_merge_with_next(header)) {
    }

#ifdef MEMORY_ZEROING_ENABLED
    header_copy = header_extract(header);
    explicit_bzero(block, header_copy.capacity.bytes);
#endif
  }
}
