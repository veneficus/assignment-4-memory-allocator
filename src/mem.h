#ifndef _MEM_H_
#define _MEM_H_

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include <sys/mman.h>

#define HEAP_START ((void *)0x04040000)

void *_malloc(size_t const query);
void _free(void *const block);
void *heap_init(size_t const initial);

#define DEBUG_FIRST_BYTES 4

#define BLOCK_MIN_CAPACITY 24

void debug_struct_info(FILE *f, void const *address);
void debug_heap(FILE *f, void const *ptr);

#endif
